﻿using System;

namespace RadchenkoEV.Lab5.Ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                double n = Convert.ToDouble(Console.ReadLine());

                do
                {
                    if (n % 2 == 0)
                    {
                        n = n / 2.0;
                        Console.WriteLine(n);
                    }
                    else
                    {
                        n = ((n * 3) + 1) / 2.0;
                        Console.WriteLine(n);
                    }
                }
                while (n != 1 );
            }
            catch (Exception )
            {
                Console.WriteLine($"Введено не корректное занчение.");
            }
        }
    }
}

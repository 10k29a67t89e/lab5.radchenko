﻿using System;

namespace Radchenko.lab5.ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                double x = Convert.ToDouble(Console.ReadLine());
                double sum = 0;
                for (double k = 1; k <= 10; k++)
                {
                    double f = Math.Sqrt(k * k + x * x) / (k * k + x * x * x);
                    sum += f;
                }
                Console.WriteLine($"Сумма ряда:{sum}");
                for (double k = 1; k <= 10; k++)
                {
                    double y = Math.Sqrt(k * k + x * x) / (k * k + x * x * x);
                    sum *= y;
                }
                Console.WriteLine($"Произведение ряда:{sum}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"ошибка ввода, введите другое значение");
            }
            
        }
    }
}

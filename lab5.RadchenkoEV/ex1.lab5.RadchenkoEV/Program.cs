﻿using System;

namespace ex1.lab5.RadchenkoEV
{
    class Program
    {
        //для циклов for
        static void Main(string[] args)
        {
            double k, f, x, q, w, e, r, t, y;
            e = -1;
            q = 1;
            w = Math.Sqrt(q * q + e * e) / (q * q + e * e * e);
            r = 1;
            y = -1;
            t = Math.Sqrt(r * r + y * y) / (r * r + y * y * y);
            x = -1;
            k = 1;
            f= Math.Sqrt(k * k + x * x) / (k * k + x * x * x);
            GGG(x, k, f);
            Console.WriteLine();
            RRR(q, w, e);
            Console.WriteLine();
            WWW(r, t, y);
        }
        static void GGG(double x, double k, double f)
        {
            double X = 0.1;
            x = -1;
            for (; x <= 1; x = x + X)
            {
                k = 1;
            double K = 1;
                for (; k <= 10; k = k + K)
                {
                    f = Math.Sqrt(k * k + x * x) / (k * k + x * x * x);
                    Console.Write($"{Math.Round(f, 2)} \t");
                }
                Console.WriteLine();
            }
        }
        //для циклов while
        static void RRR(double q,double w,double e)
        {
            double E = 0.1;
            double Q = 1;
            e = -1;
            while (  e <= 1)
            {
                q = 1;
                
                while ( q <= 10)
                { 
                    w = Math.Sqrt(q * q + e * e) / (q * q + e * e * e);
                    Console.Write($"{Math.Round(w, 2)} \t");
                    q = q + Q;
                }
                e = e + E;
                Console.WriteLine();
            }
        }
        //для цикла do...while 
        static void WWW(double r, double t, double y)
        {
            double Y = 0.1;
            double R = 1;
            y = -1;
            do
            {
                
                r = 1;
                do
                {
                    
                    t = Math.Sqrt(r * r + y * y) / (r * r + y * y * y);
                    Console.Write($"{Math.Round(t, 2)}\t");
                    r = r + R;
                }
                while (r <= 10);
                Console.WriteLine();
                y = y + Y;
            }
            while (y <= 1);
        }
        
    }
}
